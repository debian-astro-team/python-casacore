python-casacore (3.5.2-1) unstable; urgency=medium

  * New upstream version 3.5.2
  * Push Standards-Version to 4.6.2, no changes needed

 -- Ole Streicher <olebole@debian.org>  Fri, 13 Jan 2023 14:09:11 +0100

python-casacore (3.5.1-1) unstable; urgency=medium

  * New upstream version 3.5.1

 -- Ole Streicher <olebole@debian.org>  Sat, 13 Aug 2022 11:54:33 +0200

python-casacore (3.5.0-1) unstable; urgency=medium

  * New upstream version 3.5.0
  * Rediff patches
  * Push Standards-Version to 4.6.1. No changes needed
  * Change documented license to LGPL-3

 -- Ole Streicher <olebole@debian.org>  Fri, 08 Jul 2022 08:38:49 +0200

python-casacore (3.4.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Ole Streicher ]
  * New upstream version 3.4.0
  * Push Standards-Version to 4.6.0. No changes needed
  * Add "Rules-Requires-Root: no" to d/control

 -- Ole Streicher <olebole@debian.org>  Tue, 24 Aug 2021 15:30:37 +0200

python-casacore (3.3.1-1) unstable; urgency=low

  * New upstream version 3.3.1. Rediff patches
  * Push dh-compat to 13

 -- Ole Streicher <olebole@debian.org>  Fri, 22 May 2020 11:41:18 +0200

python-casacore (3.3.0-2) unstable; urgency=low

  * Add multiarch dir to library search. Fixes FTBFS on non-x86_64 archs

 -- Ole Streicher <olebole@debian.org>  Sat, 25 Apr 2020 17:05:50 +0200

python-casacore (3.3.0-1) unstable; urgency=low

  * New upstream version 3.3.0. Rediff patches.

 -- Ole Streicher <olebole@debian.org>  Sat, 25 Apr 2020 14:39:58 +0200

python-casacore (3.2.0-2) unstable; urgency=medium

  * Drop specific libcasa-python3-3 build dependency (Closes: #949546)
  * Push Standards-Version to 4.5.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 21 Jan 2020 21:49:51 +0100

python-casacore (3.2.0-1) unstable; urgency=low

  * Push Standards-Version to 4.4.1. No changes required
  * Push compat to 12. Remove d/compat
  * Add gitlab-ci.yml for salsa
  * Remove unused paragraph in d/copyright
  * New upstream version 3.2.0. Rediff patches
  * Update required min version of casacore

 -- Ole Streicher <olebole@debian.org>  Thu, 16 Jan 2020 08:39:19 +0100

python-casacore (3.0.0-1) unstable; urgency=low

  * New upstream version 3.0.0. Rediff patches
  * Drop Fix-build-and-namespace-problem.patch: applied upstream
  * Remove Python 2 package
  * Push Standards-Version to 4.2.1. No changes needed.
  * Push compat to 11

 -- Ole Streicher <olebole@debian.org>  Sat, 15 Dec 2018 15:16:36 +0100

python-casacore (2.2.1-2) unstable; urgency=medium

  [ Ole Streicher ]
  * Update VCS fields to use salsa.d.o

  [ Michael Hudson-Doyle ]
  * Fix for Boost.Python SONAME change. (Closes: #914050)

  [ Peter Michael Green ]
  * Fix build and namespace problem (Closes: #914596)
  * Update build-depends/conflicts for casacore 3

 -- Ole Streicher <olebole@debian.org>  Thu, 29 Nov 2018 19:44:19 +0100

python-casacore (2.2.1-1) unstable; urgency=low

  * New upstream version 2.2.1
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Wed, 29 Nov 2017 11:22:50 +0100

python-casacore (2.2.0-2) unstable; urgency=low

  * Set default library name for libboost-python. Fixes FTBFS on on-x86

 -- Ole Streicher <olebole@debian.org>  Wed, 08 Nov 2017 14:50:50 +0100

python-casacore (2.2.0-1) unstable; urgency=low

  [Gijs Molenaar]
  * add debian clean file
  * Imported Upstream version 2.2.0
  * Disable six patch for now

  [Ole Streicher]
  * Push Standards-Version to 4.1.1. No changes.

 -- Ole Streicher <olebole@debian.org>  Mon, 06 Nov 2017 18:28:32 +0200

python-casacore (2.1.2-5) unstable; urgency=medium

  * Run python[3]-numpy to calculate Numpy ABI dependency. (Closes: #877481)
    (LP: #1713153)
  * Push Standards-Version to 4.1.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Mon, 02 Oct 2017 12:48:20 +0200

python-casacore (2.1.2-4) unstable; urgency=low

  * Depend on python[3]-all-dev to cover all Python versions
  * Push Standards-Version to 4.0.0. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Tue, 04 Jul 2017 20:24:44 +0200

python-casacore (2.1.2-3) unstable; urgency=low

  * Use libcfitsio-dev dependency. Closes: #841216

 -- Ole Streicher <olebole@debian.org>  Wed, 19 Oct 2016 09:23:00 +0200

python-casacore (2.1.2-2) unstable; urgency=medium

  * Add debian/upstream/metadata with bug db and repository

 -- Ole Streicher <olebole@debian.org>  Mon, 26 Sep 2016 10:58:45 +0200

python-casacore (2.1.2-1) unstable; urgency=low

  * Initial version. Closes: #830118

 -- Gijs Molenaar <gijs@pythonic.nl>  Tue, 05 Jul 2016 14:47:19 +0000
