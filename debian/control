Source: python-casacore
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Gijs Molenaar <gijs@pythonic.nl>,
           Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: casacore-dev (>= 3.2~),
               debhelper-compat (= 13),
               dh-python,
               libboost-python-dev,
               libcfitsio-dev,
               python3-all,
               python3-all-dev,
               python3-numpy,
               python3-setuptools,
               python3-six,
               wcslib-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-astro-team/python-casacore
Vcs-Git: https://salsa.debian.org/debian-astro-team/python-casacore.git
Homepage: https://github.com/casacore/python-casacore
Rules-Requires-Root: no

Package: python3-casacore
Architecture: any
Depends: python3-numpy,
         python3-six,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Provides: ${python3:Provides}
Description: Python bindings to the casacore library
 The casacore package contains the core libraries of the old AIPS++/CASA
 package. This split was made to get a better separation of core
 libraries and applications. CASA is now built on top of casacore.
 .
 This is the Python wrapper around the library.
